function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports) {
    function webpackEmptyAsyncContext(req) {
      // Here Promise.resolve().then() is used instead of new Promise() to prevent
      // uncaught exception popping up in devtools
      return Promise.resolve().then(function () {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      });
    }

    webpackEmptyAsyncContext.keys = function () {
      return [];
    };

    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/about/about.component.html":
  /*!****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/admin/about/about.component.html ***!
    \****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAdminAboutAboutComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<p>about works!</p>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/dashboard/dashboard.component.html":
  /*!************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/admin/dashboard/dashboard.component.html ***!
    \************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAdminDashboardDashboardComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<nav>\r\n  <ol class=\"breadcrumb\">\r\n    <li class=\"breadcrumb-item\"><a routerLink=\"/\">Home</a></li>\r\n    <li class=\"breadcrumb-item active\">Dashboard</li>\r\n  </ol>\r\n</nav>\r\n\r\n<h5>Dashboard</h5>\r\n<h6>{{ToDay | date:\"y-M-d\"}}</h6>\r\n<div class=\"row\">\r\n  <div class=\"col-lg-3 pb-3\" style=\"background-color:#e9e6e6\">\r\n    <div class=\"row\">\r\n      <div class=\"col-11 text-white text-center mx-auto rounded pt-2 pb-2 font-weight-bold\"\r\n        style=\"background-color:#a39e9e; font-family:  'Arial Narrow Bold', sans-serif\">{{Designation | uppercase}}</div>\r\n      <div class=\"col-12 text-center mt-2\">\r\n        <img src=\"assets/user.png\" width=\"120px\">\r\n      </div>\r\n      <div class=\"col-12 text-center pt-2 pb-2 font-weight-bold\" style=\"font-family: Tahoma\">\r\n        {{Username | lowercase}}\r\n      </div>\r\n      <div class=\"col-12 text-center pt-2 pb-2\" style=\"font-family: Arial\">TEAM SUMMARY</div>\r\n      <div class=\"col-12 mb-3\">\r\n        <ul class=\"list-group\">\r\n          <li class=\"list-group-item d-flex justify-content-between align-items-center\">\r\n            NO. OF TEAM MEMBERS\r\n            <span class=\"badge badge-secondary badge-pill\" style=\"font-size:13px\">{{NoOfTeamMembers}}</span>\r\n          </li>\r\n          <li class=\"list-group-item d-flex justify-content-between align-items-center\">\r\n            TOTAL COST OF ALL PROJECTS\r\n            <span class=\"badge badge-secondary badge-pill\" style=\"font-size:13px\">$ {{TotalCostOfAllProjects}} k</span>\r\n          </li>\r\n          <li class=\"list-group-item d-flex justify-content-between align-items-center\">\r\n            PENDING TASKS\r\n            <span class=\"badge badge-secondary badge-pill\" style=\"font-size:13px\">{{PendingTasks}}</span>\r\n          </li>\r\n          <li class=\"list-group-item d-flex justify-content-between align-items-center\">\r\n            UPCOMING PROJECTS\r\n            <span class=\"badge badge-secondary badge-pill\" style=\"font-size:13px\">{{UpComingProjects | percent}}</span>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n      <div class=\"col-12 text-center pt-2 pb-2\" style=\"font-family: 'Arial Narrow'\">CLIENTS</div>\r\n      <div class=\"col-12\">\r\n        <ul class=\"list-group list-group-flush\">\r\n          <li class=\"list-group-item\" *ngFor=\"let client of Clients\">{{client | slice:0:10}}</li>\r\n        </ul>\r\n        {{TeamMembersSummary | json}}\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 pt-0 pb-2\" style=\"background-color:#e9e6e6\">\r\n        <div class=\"row mt-2\">\r\n          <div class=\"col-6 text-left\">\r\n            <div class=\"dropdown\">\r\n              <button class=\"btn btn-secondary dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton\"\r\n                data-toggle=\"dropdown\">\r\n                Project A\r\n              </button>\r\n              <div class=\"dropdown-menu\">\r\n                <a class=\"dropdown-item\" href=\"#\" *ngFor=\"let project of Projects\" onclick=\"return false\" (click)=\"onProjectChange($event)\">{{project}}</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-6 text-right\">\r\n            <div class=\"dropdown\">\r\n              <button class=\"btn btn-secondary dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton\"\r\n                data-toggle=\"dropdown\">\r\n                2019\r\n              </button>\r\n              <div class=\"dropdown-menu\">\r\n                <a class=\"dropdown-item\" href=\"#\" *ngFor=\"let year of Years\" [ngSwitch]=\"year\">\r\n                  <span *ngSwitchCase=\"'2019'\" style=\"color: green;\">{{year}}</span>\r\n                  <span *ngSwitchCase=\"'2018'\" style=\"color: blue;\">{{year}}</span>\r\n                  <span *ngSwitchCase=\"'2017'\" style=\"color: red;\">{{year}}</span>\r\n                  <span *ngSwitchDefault>{{year}}</span>\r\n                </a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-11 mx-auto mt-1 text-white text-center pt-2 pb-2 mx-auto rounded font-weight-bold\"\r\n        style=\"background-color: #718d97; font-family: 'Arial Narrow'\">PROJECT BRIEFING</div>\r\n      <div class=\"col-12\">\r\n        <table class=\"table table-borderless\">\r\n          <tr>\r\n            <td>Project Cost</td>\r\n            <td>{{ProjectCost | currency:\"USD\"}}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>Current Expenditure</td>\r\n            <td>{{CurrentExpenditure | currency:\"USD\"}}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>Available Funds</td>\r\n            <td>{{AvailableFunds | currency:\"USD\"}}</td>\r\n          </tr>\r\n        </table>\r\n      </div>\r\n      <div class=\"col-11 mx-auto mt-1 text-white text-center pt-2 pb-2 mx-2 rounded font-weight-bold\"\r\n        style=\"background-color:#718d97; font-family: 'Arial Narrow'\">TEAM MEMBERS SUMMARY</div>\r\n      <div class=\"col-12\">\r\n        <table class=\"table\">\r\n          <tr>\r\n            <th>Region</th>\r\n            <th>Team Members Count</th>\r\n            <th>Temporarily Unavailable Members</th>\r\n          </tr>\r\n          <tr *ngFor=\"let teamMemberSumary of TeamMembersSummary\">\r\n            <td>\r\n              <b>{{teamMemberSumary.Region}}</b>\r\n            </td>\r\n            <td>\r\n              {{teamMemberSumary.TeamMembersCount}}\r\n            </td>\r\n            <td>\r\n              {{teamMemberSumary.TemporarilyUnavailableMembers}}\r\n            </td>\r\n          </tr>\r\n\r\n        </table>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-3\" style=\"background-color:#e9e6e6\">\r\n    <div class=\"row\">\r\n      <div class=\"col-11 mx-auto mt-1 text-white text-center pt-1 pb-2 mx-2 rounded font-weight-bold\"\r\n        style=\"background-color:#718d97; font-family: 'Arial Narrow'\">TEAM MEMBERS</div>\r\n      <div class=\"col-lg-12 pt-2\">\r\n        <div class=\"accordion\" id=\"accordion1\">\r\n          <div class=\"card\" *ngFor=\"let teamMembersGroup of TeamMembers; let i = index\">\r\n            <div class=\"card-header bg-secondary\" id=\"card1\">\r\n              <h2 class=\"mb-0\">\r\n                <button class=\"btn btn-link text-white\" type=\"button\" data-toggle=\"collapse\"\r\n                  [attr.data-target]=\" '#cardbody' + i \">{{teamMembersGroup.Region}}</button>\r\n              </h2>\r\n            </div>\r\n            <div [id]=\" 'cardbody' + i \" class=\"collapse\" data-parent=\"#accordion1\" [ngClass]=\"(i===0) ? 'show' : ''\">\r\n              <div class=\"card-body\">\r\n\r\n                <div *ngIf=\"i === 0; then firstTemplate; else secondTemplate\">\r\n\r\n                </div>\r\n\r\n                <ng-template #firstTemplate>\r\n                  <div class=\"card\" *ngFor=\"let member of teamMembersGroup.Members\">\r\n                    <div class=\"card-header bg-primary text-white\">#{{member.ID}} {{member.Name}}</div>\r\n                    <div class=\"card-body\">{{member.Status}}</div>\r\n                  </div>\r\n                </ng-template>\r\n\r\n                <ng-template #secondTemplate>\r\n                  <table class=\"table\">\r\n                    <thead>\r\n                      <tr>\r\n                        <th>ID</th>\r\n                        <th>Name</th>\r\n                        <th>Status</th>\r\n                      </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                      <tr *ngFor=\"let member of teamMembersGroup.Members\">\r\n                        <td>{{member.ID}}</td>\r\n                        <td [style.color]=\"member.Status === 'Busy'? 'red' : 'black'\">{{member.Name}}</td>\r\n                        <td>{{member.Status}}</td>\r\n                      </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </ng-template>\r\n\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/my-profile/my-profile.component.html":
  /*!**************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/admin/my-profile/my-profile.component.html ***!
    \**************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAdminMyProfileMyProfileComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<p>my-profile works!</p>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/projects/projects.component.html":
  /*!**********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/admin/projects/projects.component.html ***!
    \**********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAdminProjectsProjectsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<h1>Projects</h1>\n<div class=\"row\">\n  <div class=\"col-8 mx-auto\">\n    <div class=\"form-group form-row\">\n      <label for=\"drpSearchBy\" class=\"col-form-label col-3\">Search By</label>\n      <div class=\"col-9\">\n        <select name=\"SearchBy\" id=\"drpSearchBy\" class=\"drpSearchBy\" [(ngModel)]=\"searchBy\">\n          <option value=\"ProjectID\">Project ID</option>\n          <option value=\"ProjectName\">Project Name</option>\n          <option value=\"DateOfStart\">Date of Start</option>\n          <option value=\"TeamSize\">Team Size</option>\n        </select>\n      </div>\n\n      <label for=\"textSearchText\" class=\"col-form-label col-3\">Search:</label>\n      <div class=\"col-9\">\n        <div class=\"input-group\">\n          <input type=\"text\" class=\"form-control\" id=\"textSearchText\" name=\"SearchText\" [(ngModel)]=\"searchText\">\n          <div class=\"input-group-append\" (click)=\"onSearchClick()\">\n            <span class=\"input-group-text\"><i class=\"fa fa-search\"></i></span>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-8 mx-auto\">\n    <button class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#newModal\" style=\"margin-bottom: 3px;\">Create Project</button>\n  </div>\n  <div class=\"col-8 mx-auto\">\n    <table class=\"table\">\n      <thead>\n        <th>Project ID</th>\n        <th>Project Name</th>\n        <th>Date of Start</th>\n        <th>Team Size</th>\n        <th>Actions</th>\n      </thead>\n      <tbody>\n        <tr *ngFor=\"let project of projects; let i = index\">\n          <td>{{project.projectID}}</td>\n          <td>{{project.projectName}}</td>\n          <td>{{project.dateOfStart}}</td>\n          <td>{{project.teamSize}}</td>\n          <td>\n            <button class=\"btn btn-info\" (click)=\"onEditClick($event, i)\" data-toggle=\"modal\" data-target=\"#editModal\">Edit</button>\n            <button class=\"btn btn-info\" (click)=\"onDeleteClick($event, i)\" data-toggle=\"modal\" data-target=\"#deleteModal\">Delete</button>\n          </td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n</div>\n\n<!-- New Project -->\n<div class=\"modal\" role=\"dialog\" id=\"newModal\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <div class=\"modal-title\">New Project</div>\n        <button class=\"close\" data-dismiss=\"modal\"><span>&times;</span></button>\n      </div>\n      <div class=\"modal-body\">\n\n        <div class=\"form-group row\">\n          <label for=\"txtNewProjectID\" class=\"col-sm-4 col-form-label\">Project ID</label>\n          <div class=\"col-sm-8\">\n            <input type=\"number\" id=\"txtNewProjectID\" class=\"form-control\" placeholder=\"Project ID\" name=\"ProjectID\"\n              [(ngModel)]=\"newProject.projectID\"\n            >\n          </div>\n        </div>\n\n        <div class=\"form-group row\">\n          <label for=\"txtNewProjectName\" class=\"col-sm-4 col-form-label\">Project Name</label>\n          <div class=\"col-sm-8\">\n            <input type=\"text\" id=\"txtNewProjectName\" class=\"form-control\" placeholder=\"Project Name\" name=\"ProjectName\"\n              [(ngModel)]=\"newProject.projectName\"\n            >\n          </div>\n        </div>\n\n        <div class=\"form-group row\">\n          <label for=\"txtNewDateOfStart\" class=\"col-sm-4 col-form-label\">Date of Start</label>\n          <div class=\"col-sm-8\">\n            <input type=\"date\" id=\"txtNewDateOfStart\" class=\"form-control\" placeholder=\"Date of Start\" name=\"DateOfStart\"\n              [(ngModel)]=\"newProject.dateOfStart\"\n            >\n          </div>\n        </div>\n\n        <div class=\"form-group row\">\n          <label for=\"txtNewTeamSize\" class=\"col-sm-4 col-form-label\">Team Size</label>\n          <div class=\"col-sm-8\">\n            <input type=\"number\" id=\"txtNewTeamSize\" class=\"form-control\" placeholder=\"Team Size\" name=\"TeamSize\"\n              [(ngModel)]=\"newProject.teamSize\"\n            >\n          </div>\n        </div>\n\n      </div>\n      <div class=\"modal-footer\">\n        <button class=\"btn btn-warning\" data-dismiss=\"modal\">Cancel</button>\n        <button class=\"btn btn-success\" data-dismiss=\"modal\" (click)=\"onSaveClick()\">Save</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<!-- Edit Project -->\n<div class=\"modal\" role=\"dialog\" id=\"editModal\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <div class=\"modal-title\">Edit Project</div>\n        <button class=\"close\" data-dismiss=\"modal\"><span>&times;</span></button>\n      </div>\n      <div class=\"modal-body\">\n\n        <div class=\"form-group row\">\n          <label for=\"txtEditProjectID\" class=\"col-sm-4 col-form-label\">Project ID</label>\n          <div class=\"col-sm-8\">\n            <input type=\"number\" id=\"txtEditProjectID\" class=\"form-control form-control-plain-text\" placeholder=\"Project ID\" name=\"ProjectID\"\n              [(ngModel)]=\"editProject.projectID\" disabled=\"disabled\"\n            >\n          </div>\n        </div>\n\n        <div class=\"form-group row\">\n          <label for=\"txtEditProjectName\" class=\"col-sm-4 col-form-label\">Project Name</label>\n          <div class=\"col-sm-8\">\n            <input type=\"text\" id=\"txtEditProjectName\" class=\"form-control\" placeholder=\"Project Name\" name=\"ProjectName\"\n              [(ngModel)]=\"editProject.projectName\"\n            >\n          </div>\n        </div>\n\n        <div class=\"form-group row\">\n          <label for=\"txtEditDateOfStart\" class=\"col-sm-4 col-form-label\">Date of Start</label>\n          <div class=\"col-sm-8\">\n            <input type=\"date\" id=\"txtEditDateOfStart\" class=\"form-control\" placeholder=\"Date of Start\" name=\"DateOfStart\"\n              [(ngModel)]=\"editProject.dateOfStart\"\n            >\n          </div>\n        </div>\n\n        <div class=\"form-group row\">\n          <label for=\"txtEditTeamSize\" class=\"col-sm-4 col-form-label\">Team Size</label>\n          <div class=\"col-sm-8\">\n            <input type=\"number\" id=\"txtEditTeamSize\" class=\"form-control\" placeholder=\"Team Size\" name=\"TeamSize\"\n              [(ngModel)]=\"editProject.teamSize\"\n            >\n          </div>\n        </div>\n\n      </div>\n      <div class=\"modal-footer\">\n        <button class=\"btn btn-warning\" data-dismiss=\"modal\">Cancel</button>\n        <button class=\"btn btn-success\" data-dismiss=\"modal\" (click)=\"onUpdateClick()\">Save</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<!-- Delete Project -->\n<div class=\"modal\" role=\"dialog\" id=\"deleteModal\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <div class=\"modal-title\">Delete Project</div>\n        <button class=\"close\" data-dismiss=\"modal\"><span>&times;</span></button>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"text-warning\">Are you sure to delete this Project?</div>\n        <div class=\"form-group row\">\n          <label for=\"txtDeleteProjectID\" class=\"col-sm-4 col-form-label\">Project ID</label>\n          <div class=\"col-sm-8\">\n            <input type=\"number\" id=\"txtDeleteProjectID\" class=\"form-control form-control-plain-text\" placeholder=\"Project ID\" name=\"ProjectID\"\n              [(ngModel)]=\"deleteProject.projectID\" disabled=\"disabled\"\n            >\n          </div>\n        </div>\n\n        <div class=\"form-group row\">\n          <label for=\"txtDeleteProjectName\" class=\"col-sm-4 col-form-label\">Project Name</label>\n          <div class=\"col-sm-8\">\n            <input type=\"text\" id=\"txtDeleteProjectName\" class=\"form-control\" placeholder=\"Project Name\" name=\"ProjectName\"\n              [(ngModel)]=\"deleteProject.projectName\" disabled=\"disabled\"\n            >\n          </div>\n        </div>\n\n        <div class=\"form-group row\">\n          <label for=\"txtDeleteDateOfStart\" class=\"col-sm-4 col-form-label\">Date of Start</label>\n          <div class=\"col-sm-8\">\n            <input type=\"date\" id=\"txtDeleteDateOfStart\" class=\"form-control\" placeholder=\"Date of Start\" name=\"DateOfStart\"\n              [(ngModel)]=\"deleteProject.dateOfStart\" disabled=\"disabled\"\n            >\n          </div>\n        </div>\n\n        <div class=\"form-group row\">\n          <label for=\"txtDeleteTeamSize\" class=\"col-sm-4 col-form-label\">Team Size</label>\n          <div class=\"col-sm-8\">\n            <input type=\"number\" id=\"txtDeleteTeamSize\" class=\"form-control\" placeholder=\"Team Size\" name=\"TeamSize\"\n              [(ngModel)]=\"deleteProject.teamSize\" disabled=\"disabled\"\n            >\n          </div>\n        </div>\n\n      </div>\n      <div class=\"modal-footer\">\n        <button class=\"btn btn-warning\" data-dismiss=\"modal\">Cancel</button>\n        <button class=\"btn btn-success\" data-dismiss=\"modal\" (click)=\"onDeleteConfirmClick()\">Delete</button>\n      </div>\n    </div>\n  </div>\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
  /*!**************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
    \**************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<nav class=\"navbar navbar-expand-sm bg-success navbar-dark\">\r\n  <a class=\"navbar-brand\" href=\"#\">\r\n    Angular Task Manager\r\n  </a>\r\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#mynav\">\r\n    <span class=\"navbar-toggler-icon\"></span>\r\n  </button>\r\n  <div class=\"collapse navbar-collapse\" id=\"mynav\">\r\n    <ul class=\"navbar-nav mr-auto\">\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"dashboard\">Dashboard</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"about\">About</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"projects\">Projects</a>\r\n      </li>\r\n    </ul>\r\n    <form class=\"form-inline my-2 my-lg-0\">\r\n      <div class=\"input-group\">\r\n        <div class=\"input-group-prepend\">\r\n          <span class=\"input-group-text\" id=\"search\"><i class=\"fa fa-search\"></i></span>\r\n        </div>\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"Search\">\r\n      </div>\r\n      <button class=\"btn btn-warning my2- my-sm-0\" type=\"button\">Search</button>\r\n    </form>\r\n  </div>\r\n</nav>\r\n\r\n<div class=\"container-fluid\">\r\n  <router-outlet></router-outlet>\r\n</div>\r\n";
    /***/
  },

  /***/
  "./node_modules/tslib/tslib.es6.js":
  /*!*****************************************!*\
    !*** ./node_modules/tslib/tslib.es6.js ***!
    \*****************************************/

  /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault, __classPrivateFieldGet, __classPrivateFieldSet */

  /***/
  function node_modulesTslibTslibEs6Js(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__extends", function () {
      return __extends;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__assign", function () {
      return _assign;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__rest", function () {
      return __rest;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__decorate", function () {
      return __decorate;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__param", function () {
      return __param;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__metadata", function () {
      return __metadata;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__awaiter", function () {
      return __awaiter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__generator", function () {
      return __generator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__exportStar", function () {
      return __exportStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__values", function () {
      return __values;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__read", function () {
      return __read;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spread", function () {
      return __spread;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () {
      return __spreadArrays;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__await", function () {
      return __await;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () {
      return __asyncGenerator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () {
      return __asyncDelegator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncValues", function () {
      return __asyncValues;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () {
      return __makeTemplateObject;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importStar", function () {
      return __importStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importDefault", function () {
      return __importDefault;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__classPrivateFieldGet", function () {
      return __classPrivateFieldGet;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__classPrivateFieldSet", function () {
      return __classPrivateFieldSet;
    });
    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0
    
    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.
    
    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    /* global Reflect, Promise */


    var _extendStatics = function extendStatics(d, b) {
      _extendStatics = Object.setPrototypeOf || {
        __proto__: []
      } instanceof Array && function (d, b) {
        d.__proto__ = b;
      } || function (d, b) {
        for (var p in b) {
          if (b.hasOwnProperty(p)) d[p] = b[p];
        }
      };

      return _extendStatics(d, b);
    };

    function __extends(d, b) {
      _extendStatics(d, b);

      function __() {
        this.constructor = d;
      }

      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var _assign = function __assign() {
      _assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];

          for (var p in s) {
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
          }
        }

        return t;
      };

      return _assign.apply(this, arguments);
    };

    function __rest(s, e) {
      var t = {};

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
      }

      if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
      }
      return t;
    }

    function __decorate(decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    }

    function __metadata(metadataKey, metadataValue) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function (resolve) {
          resolve(value);
        });
      }

      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    }

    function __generator(thisArg, body) {
      var _ = {
        label: 0,
        sent: function sent() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
          f,
          y,
          t,
          g;
      return g = {
        next: verb(0),
        "throw": verb(1),
        "return": verb(2)
      }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
        return this;
      }), g;

      function verb(n) {
        return function (v) {
          return step([n, v]);
        };
      }

      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");

        while (_) {
          try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];

            switch (op[0]) {
              case 0:
              case 1:
                t = op;
                break;

              case 4:
                _.label++;
                return {
                  value: op[1],
                  done: false
                };

              case 5:
                _.label++;
                y = op[1];
                op = [0];
                continue;

              case 7:
                op = _.ops.pop();

                _.trys.pop();

                continue;

              default:
                if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                  _ = 0;
                  continue;
                }

                if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                  _.label = op[1];
                  break;
                }

                if (op[0] === 6 && _.label < t[1]) {
                  _.label = t[1];
                  t = op;
                  break;
                }

                if (t && _.label < t[2]) {
                  _.label = t[2];

                  _.ops.push(op);

                  break;
                }

                if (t[2]) _.ops.pop();

                _.trys.pop();

                continue;
            }

            op = body.call(thisArg, _);
          } catch (e) {
            op = [6, e];
            y = 0;
          } finally {
            f = t = 0;
          }
        }

        if (op[0] & 5) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    }

    function __exportStar(m, exports) {
      for (var p in m) {
        if (!exports.hasOwnProperty(p)) exports[p] = m[p];
      }
    }

    function __values(o) {
      var s = typeof Symbol === "function" && Symbol.iterator,
          m = s && o[s],
          i = 0;
      if (m) return m.call(o);
      if (o && typeof o.length === "number") return {
        next: function next() {
          if (o && i >= o.length) o = void 0;
          return {
            value: o && o[i++],
            done: !o
          };
        }
      };
      throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
      var m = typeof Symbol === "function" && o[Symbol.iterator];
      if (!m) return o;
      var i = m.call(o),
          r,
          ar = [],
          e;

      try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) {
          ar.push(r.value);
        }
      } catch (error) {
        e = {
          error: error
        };
      } finally {
        try {
          if (r && !r.done && (m = i["return"])) m.call(i);
        } finally {
          if (e) throw e.error;
        }
      }

      return ar;
    }

    function __spread() {
      for (var ar = [], i = 0; i < arguments.length; i++) {
        ar = ar.concat(__read(arguments[i]));
      }

      return ar;
    }

    function __spreadArrays() {
      for (var s = 0, i = 0, il = arguments.length; i < il; i++) {
        s += arguments[i].length;
      }

      for (var r = Array(s), k = 0, i = 0; i < il; i++) {
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) {
          r[k] = a[j];
        }
      }

      return r;
    }

    ;

    function __await(v) {
      return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var g = generator.apply(thisArg, _arguments || []),
          i,
          q = [];
      return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i;

      function verb(n) {
        if (g[n]) i[n] = function (v) {
          return new Promise(function (a, b) {
            q.push([n, v, a, b]) > 1 || resume(n, v);
          });
        };
      }

      function resume(n, v) {
        try {
          step(g[n](v));
        } catch (e) {
          settle(q[0][3], e);
        }
      }

      function step(r) {
        r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
      }

      function fulfill(value) {
        resume("next", value);
      }

      function reject(value) {
        resume("throw", value);
      }

      function settle(f, v) {
        if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]);
      }
    }

    function __asyncDelegator(o) {
      var i, p;
      return i = {}, verb("next"), verb("throw", function (e) {
        throw e;
      }), verb("return"), i[Symbol.iterator] = function () {
        return this;
      }, i;

      function verb(n, f) {
        i[n] = o[n] ? function (v) {
          return (p = !p) ? {
            value: __await(o[n](v)),
            done: n === "return"
          } : f ? f(v) : v;
        } : f;
      }
    }

    function __asyncValues(o) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var m = o[Symbol.asyncIterator],
          i;
      return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i);

      function verb(n) {
        i[n] = o[n] && function (v) {
          return new Promise(function (resolve, reject) {
            v = o[n](v), settle(resolve, reject, v.done, v.value);
          });
        };
      }

      function settle(resolve, reject, d, v) {
        Promise.resolve(v).then(function (v) {
          resolve({
            value: v,
            done: d
          });
        }, reject);
      }
    }

    function __makeTemplateObject(cooked, raw) {
      if (Object.defineProperty) {
        Object.defineProperty(cooked, "raw", {
          value: raw
        });
      } else {
        cooked.raw = raw;
      }

      return cooked;
    }

    ;

    function __importStar(mod) {
      if (mod && mod.__esModule) return mod;
      var result = {};
      if (mod != null) for (var k in mod) {
        if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
      }
      result.default = mod;
      return result;
    }

    function __importDefault(mod) {
      return mod && mod.__esModule ? mod : {
        default: mod
      };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
      if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
      }

      return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
      if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
      }

      privateMap.set(receiver, value);
      return value;
    }
    /***/

  },

  /***/
  "./src/app/admin/about/about.component.scss":
  /*!**************************************************!*\
    !*** ./src/app/admin/about/about.component.scss ***!
    \**************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAdminAboutAboutComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2Fib3V0L2Fib3V0LmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/admin/about/about.component.ts":
  /*!************************************************!*\
    !*** ./src/app/admin/about/about.component.ts ***!
    \************************************************/

  /*! exports provided: AboutComponent */

  /***/
  function srcAppAdminAboutAboutComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AboutComponent", function () {
      return AboutComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var AboutComponent = /*#__PURE__*/function () {
      function AboutComponent() {
        _classCallCheck(this, AboutComponent);
      }

      _createClass(AboutComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return AboutComponent;
    }();

    AboutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'about',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./about.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/about/about.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./about.component.scss */
      "./src/app/admin/about/about.component.scss")).default]
    })], AboutComponent);
    /***/
  },

  /***/
  "./src/app/admin/admin.module.ts":
  /*!***************************************!*\
    !*** ./src/app/admin/admin.module.ts ***!
    \***************************************/

  /*! exports provided: AdminModule */

  /***/
  function srcAppAdminAdminModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AdminModule", function () {
      return AdminModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./dashboard/dashboard.component */
    "./src/app/admin/dashboard/dashboard.component.ts");
    /* harmony import */


    var _about_about_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./about/about.component */
    "./src/app/admin/about/about.component.ts");
    /* harmony import */


    var _my_profile_my_profile_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./my-profile/my-profile.component */
    "./src/app/admin/my-profile/my-profile.component.ts");
    /* harmony import */


    var _dashboard_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../dashboard.service */
    "./src/app/dashboard.service.ts");
    /* harmony import */


    var _projects_projects_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./projects/projects.component */
    "./src/app/admin/projects/projects.component.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");

    var AdminModule = function AdminModule() {
      _classCallCheck(this, AdminModule);
    };

    AdminModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"], _about_about_component__WEBPACK_IMPORTED_MODULE_4__["AboutComponent"], _my_profile_my_profile_component__WEBPACK_IMPORTED_MODULE_5__["MyProfileComponent"], _projects_projects_component__WEBPACK_IMPORTED_MODULE_7__["ProjectsComponent"]],
      exports: [_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"], _about_about_component__WEBPACK_IMPORTED_MODULE_4__["AboutComponent"], _my_profile_my_profile_component__WEBPACK_IMPORTED_MODULE_5__["MyProfileComponent"], _projects_projects_component__WEBPACK_IMPORTED_MODULE_7__["ProjectsComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"]],
      providers: [_dashboard_service__WEBPACK_IMPORTED_MODULE_6__["DashboardService"]]
    })], AdminModule);
    /***/
  },

  /***/
  "./src/app/admin/dashboard/dashboard.component.scss":
  /*!**********************************************************!*\
    !*** ./src/app/admin/dashboard/dashboard.component.scss ***!
    \**********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAdminDashboardDashboardComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/admin/dashboard/dashboard.component.ts":
  /*!********************************************************!*\
    !*** ./src/app/admin/dashboard/dashboard.component.ts ***!
    \********************************************************/

  /*! exports provided: DashboardComponent */

  /***/
  function srcAppAdminDashboardDashboardComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardComponent", function () {
      return DashboardComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_app_dashboard_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/dashboard.service */
    "./src/app/dashboard.service.ts");

    var DashboardComponent = /*#__PURE__*/function () {
      function DashboardComponent(dashboardService) {
        _classCallCheck(this, DashboardComponent);

        this.dashboardService = dashboardService;
        this.Years = [];
        this.TeamMembersSummary = [];
        this.TeamMembers = [];
      }

      _createClass(DashboardComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.Designation = "Team Leader";
          this.Username = "Scott Smith";
          this.NoOfTeamMembers = 67;
          this.TotalCostOfAllProjects = 240;
          this.PendingTasks = 15;
          this.UpComingProjects = 0.2;
          this.ProjectCost = 2113507;
          this.CurrentExpenditure = 96788;
          this.AvailableFunds = 52536;
          this.ToDay = new Date();
          this.Clients = ["ABC Infotech Ltd.", "DEF Software Solutions", "GHI Industries"];
          this.Projects = ["Project A", "Project B", "Project C", "Project D"];

          for (var i = 2019; i >= 2010; i--) {
            this.Years.push(i);
          }

          this.TeamMembersSummary = this.dashboardService.getTeamMembersSummary();
          this.TeamMembers = [{
            Region: "East",
            Members: [{
              ID: 1,
              Name: "Ford",
              Status: "Available"
            }, {
              ID: 2,
              Name: "Miller",
              Status: "Available"
            }, {
              ID: 3,
              Name: "Jones",
              Status: "Busy"
            }, {
              ID: 4,
              Name: "James",
              Status: "Busy"
            }]
          }, {
            Region: "West",
            Members: [{
              ID: 5,
              Name: "Anna",
              Status: "Available"
            }, {
              ID: 6,
              Name: "Arun",
              Status: "Available"
            }, {
              ID: 7,
              Name: "Varun",
              Status: "Busy"
            }, {
              ID: 8,
              Name: "Jasmine",
              Status: "Busy"
            }]
          }, {
            Region: "South",
            Members: [{
              ID: 9,
              Name: "Krishna",
              Status: "Available"
            }, {
              ID: 10,
              Name: "Mohan",
              Status: "Available"
            }, {
              ID: 11,
              Name: "Raju",
              Status: "Busy"
            }, {
              ID: 12,
              Name: "Farooq",
              Status: "Busy"
            }]
          }, {
            Region: "North",
            Members: [{
              ID: 13,
              Name: "Jacob",
              Status: "Available"
            }, {
              ID: 14,
              Name: "Smith",
              Status: "Available"
            }, {
              ID: 15,
              Name: "Jones",
              Status: "Busy"
            }, {
              ID: 16,
              Name: "James",
              Status: "Busy"
            }]
          }];
        }
      }, {
        key: "onProjectChange",
        value: function onProjectChange($event) {
          if ($event.target.innerHTML === "Project A") {
            this.ProjectCost = 2113507;
            this.CurrentExpenditure = 96788;
            this.AvailableFunds = 52536;
          } else if ($event.target.innerHTML === "Project B") {
            this.ProjectCost = 7464457;
            this.CurrentExpenditure = 8663;
            this.AvailableFunds = 245443;
          } else if ($event.target.innerHTML === "Project C") {
            this.ProjectCost = 345879;
            this.CurrentExpenditure = 21345669;
            this.AvailableFunds = 7898;
          } else if ($event.target.innerHTML === "Project D") {
            this.ProjectCost = 787223;
            this.CurrentExpenditure = 53453;
            this.AvailableFunds = 234325;
          }
        }
      }]);

      return DashboardComponent;
    }();

    DashboardComponent.ctorParameters = function () {
      return [{
        type: src_app_dashboard_service__WEBPACK_IMPORTED_MODULE_2__["DashboardService"]
      }];
    };

    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-dashboard',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./dashboard.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/dashboard/dashboard.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./dashboard.component.scss */
      "./src/app/admin/dashboard/dashboard.component.scss")).default]
    })], DashboardComponent);
    /***/
  },

  /***/
  "./src/app/admin/my-profile/my-profile.component.scss":
  /*!************************************************************!*\
    !*** ./src/app/admin/my-profile/my-profile.component.scss ***!
    \************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAdminMyProfileMyProfileComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL215LXByb2ZpbGUvbXktcHJvZmlsZS5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/admin/my-profile/my-profile.component.ts":
  /*!**********************************************************!*\
    !*** ./src/app/admin/my-profile/my-profile.component.ts ***!
    \**********************************************************/

  /*! exports provided: MyProfileComponent */

  /***/
  function srcAppAdminMyProfileMyProfileComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyProfileComponent", function () {
      return MyProfileComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var MyProfileComponent = /*#__PURE__*/function () {
      function MyProfileComponent() {
        _classCallCheck(this, MyProfileComponent);
      }

      _createClass(MyProfileComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return MyProfileComponent;
    }();

    MyProfileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-my-profile',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./my-profile.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/my-profile/my-profile.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./my-profile.component.scss */
      "./src/app/admin/my-profile/my-profile.component.scss")).default]
    })], MyProfileComponent);
    /***/
  },

  /***/
  "./src/app/admin/projects/projects.component.scss":
  /*!********************************************************!*\
    !*** ./src/app/admin/projects/projects.component.scss ***!
    \********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAdminProjectsProjectsComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".table .btn-info {\n  margin-right: 3px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vcHJvamVjdHMvRTpcXFByb2plY3RzXFx0YXNrbWFuYWdlclxcZnJvbnRlbmQvc3JjXFxhcHBcXGFkbWluXFxwcm9qZWN0c1xccHJvamVjdHMuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2FkbWluL3Byb2plY3RzL3Byb2plY3RzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsaUJBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3Byb2plY3RzL3Byb2plY3RzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRhYmxlIHtcclxuICAuYnRuLWluZm8ge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAzcHg7XHJcbiAgfVxyXG59XHJcbiIsIi50YWJsZSAuYnRuLWluZm8ge1xuICBtYXJnaW4tcmlnaHQ6IDNweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/admin/projects/projects.component.ts":
  /*!******************************************************!*\
    !*** ./src/app/admin/projects/projects.component.ts ***!
    \******************************************************/

  /*! exports provided: ProjectsComponent */

  /***/
  function srcAppAdminProjectsProjectsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProjectsComponent", function () {
      return ProjectsComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _projects_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../projects.service */
    "./src/app/projects.service.ts");
    /* harmony import */


    var src_app_project__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/project */
    "./src/app/project.ts");

    var ProjectsComponent = /*#__PURE__*/function () {
      function ProjectsComponent(projectsService) {
        _classCallCheck(this, ProjectsComponent);

        this.projectsService = projectsService;
        this.newProject = new src_app_project__WEBPACK_IMPORTED_MODULE_3__["Project"]();
        this.editProject = new src_app_project__WEBPACK_IMPORTED_MODULE_3__["Project"]();
        this.deleteProject = new src_app_project__WEBPACK_IMPORTED_MODULE_3__["Project"]();
        this.editIndex = null;
        this.deleteIndex = null;
        this.searchBy = "ProjectName";
        this.searchText = "";
      }

      _createClass(ProjectsComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.projectsService.getAllProjects().subscribe(function (response) {
            _this.projects = response;
          });
        }
      }, {
        key: "onSaveClick",
        value: function onSaveClick() {
          var _this2 = this;

          this.projectsService.insertProject(this.newProject).subscribe(function (responce) {
            //Add Project to Grid
            var p = new src_app_project__WEBPACK_IMPORTED_MODULE_3__["Project"]();
            p.projectID = responce.projectID;
            p.projectName = responce.projectName;
            p.dateOfStart = responce.dateOfStart;
            p.teamSize = responce.teamSize;

            _this2.projects.push(p); //Clear Project Dialog form


            _this2.newProject.projectID = null;
            _this2.newProject.projectName = null;
            _this2.newProject.dateOfStart = null;
            _this2.newProject.teamSize = null;
          }, function (error) {
            console.error(error);
          });
        }
      }, {
        key: "onEditClick",
        value: function onEditClick(event, index) {
          this.editProject.projectID = this.projects[index].projectID;
          this.editProject.projectName = this.projects[index].projectName;
          this.editProject.dateOfStart = this.projects[index].dateOfStart;
          this.editProject.teamSize = this.projects[index].teamSize;
          this.editIndex = index;
        }
      }, {
        key: "onUpdateClick",
        value: function onUpdateClick() {
          var _this3 = this;

          this.projectsService.updateProject(this.editProject).subscribe(function (responce) {
            var p = new src_app_project__WEBPACK_IMPORTED_MODULE_3__["Project"]();
            p.projectID = responce.projectID;
            p.projectName = responce.projectName;
            p.dateOfStart = responce.dateOfStart;
            p.teamSize = responce.teamSize;
            _this3.projects[_this3.editIndex] = p; //Clear Edit Dialog form

            _this3.editProject.projectID = null;
            _this3.editProject.projectName = null;
            _this3.editProject.dateOfStart = null;
            _this3.editProject.teamSize = null;
          }, function (error) {
            console.error(error);
          });
        }
      }, {
        key: "onDeleteClick",
        value: function onDeleteClick(event, index) {
          this.deleteIndex = index;
          this.deleteProject.projectID = this.projects[index].projectID;
          this.deleteProject.projectName = this.projects[index].projectName;
          this.deleteProject.dateOfStart = this.projects[index].dateOfStart;
          this.deleteProject.teamSize = this.projects[index].teamSize;
        }
      }, {
        key: "onDeleteConfirmClick",
        value: function onDeleteConfirmClick() {
          var _this4 = this;

          this.projectsService.deleteProject(this.deleteProject.projectID).subscribe(function (responce) {
            _this4.projects.splice(_this4.deleteIndex, 1); //Clear form


            _this4.deleteProject.projectID = null;
            _this4.deleteProject.projectName = null;
            _this4.deleteProject.dateOfStart = null;
            _this4.deleteProject.teamSize = null;
          }, function (error) {
            console.error(error);
          });
        }
      }, {
        key: "onSearchClick",
        value: function onSearchClick() {
          var _this5 = this;

          this.projectsService.searchProjects(this.searchBy, this.searchText).subscribe(function (responce) {
            _this5.projects = responce;
          }, function (error) {
            console.error(error);
          });
        }
      }]);

      return ProjectsComponent;
    }();

    ProjectsComponent.ctorParameters = function () {
      return [{
        type: _projects_service__WEBPACK_IMPORTED_MODULE_2__["ProjectsService"]
      }];
    };

    ProjectsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-projects',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./projects.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/admin/projects/projects.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./projects.component.scss */
      "./src/app/admin/projects/projects.component.scss")).default]
    })], ProjectsComponent);
    /***/
  },

  /***/
  "./src/app/app-routing.module.ts":
  /*!***************************************!*\
    !*** ./src/app/app-routing.module.ts ***!
    \***************************************/

  /*! exports provided: AppRoutingModule */

  /***/
  function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
      return AppRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _admin_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./admin/dashboard/dashboard.component */
    "./src/app/admin/dashboard/dashboard.component.ts");
    /* harmony import */


    var _admin_about_about_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./admin/about/about.component */
    "./src/app/admin/about/about.component.ts");
    /* harmony import */


    var _admin_projects_projects_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./admin/projects/projects.component */
    "./src/app/admin/projects/projects.component.ts");

    var routes = [{
      path: "dashboard",
      component: _admin_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"]
    }, {
      path: "about",
      component: _admin_about_about_component__WEBPACK_IMPORTED_MODULE_4__["AboutComponent"]
    }, {
      path: "projects",
      component: _admin_projects_projects_component__WEBPACK_IMPORTED_MODULE_5__["ProjectsComponent"]
    }, {
      path: "",
      redirectTo: "dashboard",
      pathMatch: "full"
    }];

    var AppRoutingModule = function AppRoutingModule() {
      _classCallCheck(this, AppRoutingModule);
    };

    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AppRoutingModule);
    /***/
  },

  /***/
  "./src/app/app.component.scss":
  /*!************************************!*\
    !*** ./src/app/app.component.scss ***!
    \************************************/

  /*! exports provided: default */

  /***/
  function srcAppAppComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var AppComponent = function AppComponent() {
      _classCallCheck(this, AppComponent);

      this.title = 'TaskManager';
    };

    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-root',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./app.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./app.component.scss */
      "./src/app/app.component.scss")).default]
    })], AppComponent);
    /***/
  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app-routing.module */
    "./src/app/app-routing.module.ts");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _admin_admin_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./admin/admin.module */
    "./src/app/admin/admin.module.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var AppModule = function AppModule() {
      _classCallCheck(this, AppModule);
    };

    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
      declarations: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]],
      imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"], _admin_admin_module__WEBPACK_IMPORTED_MODULE_5__["AdminModule"]],
      providers: [],
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })], AppModule);
    /***/
  },

  /***/
  "./src/app/dashboard.service.ts":
  /*!**************************************!*\
    !*** ./src/app/dashboard.service.ts ***!
    \**************************************/

  /*! exports provided: DashboardService */

  /***/
  function srcAppDashboardServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardService", function () {
      return DashboardService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var DashboardService = /*#__PURE__*/function () {
      function DashboardService() {
        _classCallCheck(this, DashboardService);
      }

      _createClass(DashboardService, [{
        key: "getTeamMembersSummary",
        value: function getTeamMembersSummary() {
          var TeamMembersSummary = [{
            Region: "East",
            TeamMembersCount: 20,
            TemporarilyUnavailableMembers: 4
          }, {
            Region: "West",
            TeamMembersCount: 15,
            TemporarilyUnavailableMembers: 8
          }, {
            Region: "South",
            TeamMembersCount: 17,
            TemporarilyUnavailableMembers: 1
          }, {
            Region: "North",
            TeamMembersCount: 15,
            TemporarilyUnavailableMembers: 6
          }];
          return TeamMembersSummary;
        }
      }]);

      return DashboardService;
    }();

    DashboardService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], DashboardService);
    /***/
  },

  /***/
  "./src/app/project.ts":
  /*!****************************!*\
    !*** ./src/app/project.ts ***!
    \****************************/

  /*! exports provided: Project */

  /***/
  function srcAppProjectTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Project", function () {
      return Project;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var Project = function Project() {
      _classCallCheck(this, Project);

      this.projectID = 0;
      this.projectName = null;
      this.dateOfStart = null;
      this.teamSize = 0;
    };
    /***/

  },

  /***/
  "./src/app/projects.service.ts":
  /*!*************************************!*\
    !*** ./src/app/projects.service.ts ***!
    \*************************************/

  /*! exports provided: ProjectsService */

  /***/
  function srcAppProjectsServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProjectsService", function () {
      return ProjectsService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var ProjectsService = /*#__PURE__*/function () {
      function ProjectsService(httpClient) {
        _classCallCheck(this, ProjectsService);

        this.httpClient = httpClient;
      }

      _createClass(ProjectsService, [{
        key: "getAllProjects",
        value: function getAllProjects() {
          return this.httpClient.get("/api/projects", {
            responseType: "json"
          }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) {
            for (var i = 0; i < data.length; i++) {
              data[i].teamSize = data[i].teamSize * 100;
            }

            return data;
          }));
        }
      }, {
        key: "insertProject",
        value: function insertProject(newProject) {
          return this.httpClient.post("/api/projects", newProject, {
            responseType: "json"
          });
        }
      }, {
        key: "updateProject",
        value: function updateProject(existingProject) {
          return this.httpClient.put("/api/projects", existingProject, {
            responseType: "json"
          });
        }
      }, {
        key: "deleteProject",
        value: function deleteProject(ProjectID) {
          return this.httpClient.delete("/api/projects?ProjectID=" + ProjectID);
        }
      }, {
        key: "searchProjects",
        value: function searchProjects(searchBy, searchText) {
          return this.httpClient.get("/api/projects/search/" + searchBy + "/" + searchText, {
            responseType: "json"
          });
        }
      }]);

      return ProjectsService;
    }();

    ProjectsService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    ProjectsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], ProjectsService);
    /***/
  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js"); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    var environment = {
      production: false
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/platform-browser-dynamic */
    "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
    }

    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"]).catch(function (err) {
      return console.error(err);
    });
    /***/
  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! E:\Projects\taskmanager\frontend\src\main.ts */
    "./src/main.ts");
    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map