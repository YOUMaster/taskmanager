import { Component, OnInit } from '@angular/core';
import { ProjectsService } from '../../projects.service';
import { Project } from 'src/app/project';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  projects: Project[];
  newProject: Project = new Project();
  editProject: Project = new Project();
  deleteProject: Project = new Project();
  editIndex: number = null;
  deleteIndex: number = null;
  searchBy: string = "ProjectName";
  searchText: string = "";
  constructor(private projectsService: ProjectsService) { }

  ngOnInit() {
    this.projectsService.getAllProjects().subscribe((response: Project[]) => {
      this.projects = response;
    });
  }

  onSaveClick() {
    this.projectsService.insertProject(this.newProject).subscribe((responce) => {
      //Add Project to Grid
      var p: Project = new Project();
      p.projectID = responce.projectID;
      p.projectName = responce.projectName;
      p.dateOfStart = responce.dateOfStart;
      p.teamSize = responce.teamSize;
      this.projects.push(p);

      //Clear Project Dialog form
      this.newProject.projectID = null;
      this.newProject.projectName = null;
      this.newProject.dateOfStart= null;
      this.newProject.teamSize = null;
    }, (error) => {
      console.error(error);
    });
  }

  onEditClick(event, index: number) {
    this.editProject.projectID = this.projects[index].projectID;
    this.editProject.projectName = this.projects[index].projectName;
    this.editProject.dateOfStart = this.projects[index].dateOfStart;
    this.editProject.teamSize = this.projects[index].teamSize;
    this.editIndex = index;
  }

  onUpdateClick() {
    this.projectsService.updateProject(this.editProject).subscribe((responce: Project) => {
      var p: Project = new Project();
      p.projectID = responce.projectID;
      p.projectName = responce.projectName;
      p.dateOfStart = responce.dateOfStart;
      p.teamSize = responce.teamSize;
      this.projects[this.editIndex] = p;

      //Clear Edit Dialog form
      this.editProject.projectID = null;
      this.editProject.projectName = null;
      this.editProject.dateOfStart= null;
      this.editProject.teamSize = null;
    }, (error) => {
      console.error(error);
    });
  }

  onDeleteClick(event, index: number) {
    this.deleteIndex = index;
    this.deleteProject.projectID = this.projects[index].projectID;
    this.deleteProject.projectName = this.projects[index].projectName;
    this.deleteProject.dateOfStart = this.projects[index].dateOfStart;
    this.deleteProject.teamSize = this.projects[index].teamSize;
  }

  onDeleteConfirmClick() {
    this.projectsService.deleteProject(this.deleteProject.projectID).subscribe((responce) => {
      this.projects.splice(this.deleteIndex, 1);

      //Clear form
      this.deleteProject.projectID = null;
      this.deleteProject.projectName = null;
      this.deleteProject.dateOfStart= null;
      this.deleteProject.teamSize = null;
    }, (error) => {
      console.error(error);
    });
  }

  onSearchClick() {
    this.projectsService.searchProjects(this.searchBy, this.searchText).subscribe((responce: Project[]) => {
      this.projects = responce;
    }, (error) => {
      console.error(error);
    });
  }
}
